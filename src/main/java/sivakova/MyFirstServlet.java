package sivakova;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyFirstServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        response.setStatus(200);
        response.setHeader("Content-Type", "text/html;charset=UTF-8");
        try {
            response.getWriter().write("<p>Введите текст в поле.</p>");
            response.getWriter().write("<form method=\"post\">");
            response.getWriter().write("<input type=\"text\" name=\"message\"/>");
            response.getWriter().write("<input type=\"submit\"/>");
            response.getWriter().write("</form>");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        try {
            String message = request.getParameter("message");
            response.getWriter().write("<p>" + message + "</p>");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
